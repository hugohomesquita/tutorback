from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class LessonManager(models.Manager):

    def courses(self):
        return self.get_queryset().filter(is_course=True)

class Lesson(models.Model):
    name = models.CharField(max_length=150)
    is_course = models.BooleanField(default=False)
    objects = LessonManager()

    def __str__(self):
        return self.name

    def requisites(self):
        return LessonRelation.objects.filter(lesson_father=self)

class LessonRelation(models.Model):
    lesson_father = models.ForeignKey(Lesson, related_name='father')
    lesson_child = models.ForeignKey(Lesson, related_name='child')
    weight = models.PositiveSmallIntegerField()
    order = models.PositiveSmallIntegerField()

    def __str__(self):
        return str(self.lesson_child)

class LessonWatch(models.Model):
    lesson = models.ForeignKey(Lesson)
    user = models.ForeignKey(User)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.lesson.name + " - " + str(self.user) + " - " + str(self.created_at)

class LessonActivity(models.Model):
    lesson = models.ForeignKey(Lesson)
    user = models.ForeignKey(User)
    score = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.lesson.name + " - " + str(self.user)

class LessonHelp(models.Model):
    lesson = models.ForeignKey(Lesson)
    user = models.ForeignKey(User)
    number = models.SmallIntegerField(default=0)

    def __str__(self):
        return self.lesson.name + " - " + str(self.user) + " " + str(self.number)