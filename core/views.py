from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from core.models import Lesson, LessonWatch, LessonRelation, LessonActivity
from django.contrib.auth.models import User

# Create your views here.

def index(request):
    context = {
        'courses': Lesson.objects.courses
    }

    if not request.user.is_authenticated():
        return HttpResponseRedirect('/admin/login/?next=/')
        #return HttpResponseRedirect(reverse('admin:login'))

    return render(request, 'index/dashboard.html', context=context)


def watch(request, lesson_id):
    lesson = get_object_or_404(Lesson, pk=lesson_id)
    user = request.user
    lesson_watch = LessonWatch()
    lesson_watch.lesson = lesson
    lesson_watch.user = user
    lesson_watch.save()

def lesson_mount(request, lesson_id):
    lesson = get_object_or_404(Lesson, pk=lesson_id)
    A = {}
