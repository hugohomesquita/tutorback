from django import template
from core.business import course_knowledge, Know, course_trail
from core.models import *
from datetime import datetime
register = template.Library()

@register.simple_tag()
def get_course_knowledge(course, user):
    know = course_knowledge(lesson_id=course.id, user_id=user.id)
    return know

@register.simple_tag()
def get_weight(father, child):
    relation = LessonRelation.objects.get(lesson_child=child, lesson_father=father)
    return relation.weight

@register.simple_tag()
def get_last_watch(user, child):
    last_watch = LessonWatch.objects.filter(user_id=user.id, lesson_id=child.id).last() or 0
    if last_watch:
        time_now = datetime.now().replace(tzinfo=None)
        time_last = last_watch.created_at.replace(tzinfo=None)
        last_watch = (time_now - time_last).days
    return last_watch

@register.simple_tag()
def get_helps(user, child):
    number = 0
    help = LessonHelp.objects.filter(user_id=user.id, lesson_id=child.id).first() or 0
    if help:
        number = help.number
    return number


@register.simple_tag()
def get_course_trail(course, user):
    trail = course_trail(lesson_id=course.id, user_id=user.id)
    return trail