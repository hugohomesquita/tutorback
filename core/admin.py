from django.contrib import admin
from core.models import *

# Register your models here.
admin.site.site_header = 'Administration'

class LessonRelationInline(admin.TabularInline):
    model = LessonRelation
    fk_name = 'lesson_father'
    min_num = 0
    extra = 1

class LessonAdmin(admin.ModelAdmin):
    inlines = [LessonRelationInline]

admin.site.register(LessonRelation)
admin.site.register(Lesson, LessonAdmin)
admin.site.register(LessonWatch)
admin.site.register(LessonActivity)
admin.site.register(LessonHelp)