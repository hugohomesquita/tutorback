from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from datetime import datetime

from core.models import Lesson, LessonActivity, LessonWatch, LessonHelp, LessonRelation

class Know:
    weight = 0
    score = 0
    lesson = None

    def add_score(self, know):
        self.score += know.score

    def __str__(self):
        return str(self.lesson) + " " + str(self.score) + "% " + str(self.weight) + "%"

def recursive_know(user_id, lesson):

    know = Know()
    know.lesson = lesson
    requisites = lesson.requisites()

    for child in requisites:
        know.score += recursive_know(user_id, child.lesson_child).score * child.weight  #get_knowledge(user_id, child.lesson_child.id)
        know.weight += child.weight

    if not len(requisites):
        know.score = get_knowledge(user_id, lesson.id)
    else:
        pai_know = get_knowledge(user_id, lesson.id)
        pai_weight = 100 - know.weight
        pai_know = (pai_know * pai_weight)
        result = (pai_know + know.score) / 100
        know.score = result

    return know

def get_knowledge(user_id, lesson_id):
    user = get_object_or_404(User, pk=user_id)
    lesson = get_object_or_404(Lesson, pk=lesson_id)

    lessons_activities = LessonActivity.objects.filter(lesson=lesson, user=user)
    score_count = 0
    lesson_count = len(lessons_activities)
    for activity in lessons_activities:
        score_count += activity.score

    return (score_count / lesson_count) if lesson_count else 0

def course_knowledge(user_id, lesson_id):
    lesson = get_object_or_404(Lesson, pk=lesson_id)
    return recursive_know(user_id, lesson)

def recursive_childs(A, lesson):
    if not lesson:
        return None
    A.append(lesson)
    childs = lesson.requisites()
    for child in childs:
        new_childs = child.lesson_child
        recursive_childs(A, new_childs)

class Path:
    lesson = None
    watch = 0

    def __str__(self):
        return str(self.lesson) + " " + str(self.watch)

class Trail:
    path = []

def course_trail(user_id, lesson_id):
    #Obter requisitos da aula selecionada | obter os pais da aula
    lesson = get_object_or_404(Lesson, pk=lesson_id)
    lesson_requisites = []
    TRAIL = []
    recursive_childs(lesson_requisites, lesson)

    for lesson in lesson_requisites:
        new_path = Path()
        new_path.lesson = lesson

        #Obter resultado de HAND
        know = get_knowledge(user_id, lesson.id)

        #Obter tempo da última vez que ele assistiu
        last_watch = LessonWatch.objects.filter(user_id=user_id, lesson_id=lesson.id).last() or 0
        if last_watch:
            time_now = datetime.now().replace(tzinfo=None)
            time_last = last_watch.created_at.replace(tzinfo=None)
            last_watch = (time_now - time_last).days

        #Obter se ele teve dúvidas
        helps = LessonHelp.objects.filter(user_id=user_id, lesson_id=lesson.id).first() or 0

        watch = 0
        #Condições para definir se deve assistir ou não

        ##INICIANTE
        if know < 70:
            watch = 2
        else:
            if know < 80: ##iniciante
                ##ultima vez que assitiu
                if last_watch <= 2:
                    if helps == 0:
                        watch = 0
                    elif(2 < helps < 4):
                        watch = 0
                    else:
                        watch = 2
                elif (2 < last_watch < 4):
                    if helps == 0:
                        watch = 0
                    elif(2 < helps < 4):
                        watch = 0
                    else:
                        watch = 2
                else:
                    if helps == 0:
                        watch = 1
                    elif(2 < helps < 4):
                        watch = 1
                    else:
                        watch = 2
                print("iniciante")
            elif (80 <= know < 90): ##intermediário
                ##ultima vez que assitiu
                if last_watch <= 2:
                    if helps == 0:
                        watch = 0
                    elif(2 < helps < 4):
                        watch = 0
                    else:
                        watch = 0
                elif (2 < last_watch < 4):
                    if helps == 0:
                        watch = 0
                    elif(2 < helps < 4):
                        watch = 0
                    else:
                        watch = 1
                else:
                    if helps == 0:
                        watch = 0
                    elif(2 < helps < 4):
                        watch = 1
                    else:
                        watch = 2
                print("intermediário")
            else: ##Avançado
                if last_watch <= 2:
                    if helps == 0:
                        watch = 0
                    elif(2 < helps < 4):
                        watch = 0
                    else:
                        watch = 0
                elif (2 < last_watch < 4):
                    if helps == 0:
                        watch = 0
                    elif(2 < helps < 4):
                        watch = 0
                    else:
                        watch = 1
                else:
                    if helps == 0:
                        watch = 0
                    elif(2 < helps < 4):
                        watch = 1
                    else:
                        watch = 1
                print("Avançado")

        new_path.watch = watch

        TRAIL.append(new_path)
    return TRAIL