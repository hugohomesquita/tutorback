from django.contrib import auth

from rest_framework.reverse import reverse
from rest_framework import serializers

from core.models import Lesson, LessonRelation

class LessonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lesson
        fields = ('pk', 'name')

class LessonRelationSerializer(serializers.ModelSerializer):
    lesson = serializers.SerializerMethodField()

    class Meta:
        model = LessonRelation
        fields = ('lesson', 'weight', 'order')

    def get_lesson(self, lesson):
        return LessonSerializer(lesson.lesson_child).data

class CourseSerializer(serializers.ModelSerializer):
    requisites = serializers.SerializerMethodField()

    class Meta:
        model = Lesson
        fields = ('pk', 'name','requisites')

    def get_requisites(self, course):
        return LessonRelationSerializer(course.requisites(), many=True).data