from django.conf.urls import url, include
from rest_framework import routers
from api import views

router = routers.DefaultRouter()
router.register(r'lesson', views.LessonViewSet, 'lesson')
router.register(r'course', views.CourseViewSet, 'course')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    #url(r'^$', 'api.views.test', name='index')
    url(r'^', include(router.urls)),
]