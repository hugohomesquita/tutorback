from django.shortcuts import render
from rest_framework import viewsets

from core.models import Lesson, LessonRelation
from api.serializers import LessonSerializer, CourseSerializer, LessonRelationSerializer

# Create your views here.

class LessonViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Lesson.objects.all()
    serializer_class = LessonSerializer


class CourseViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Lesson.objects.courses()
    serializer_class = CourseSerializer